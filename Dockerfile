FROM python:slim-buster as base
WORKDIR /opt/todoapp
RUN apt-get update
RUN apt-get install -y curl
#RUN apt-get install -y python3
RUN curl -sSL https://install.python-poetry.org | python3 -
COPY ./pyproject.toml ./pyproject.toml
COPY ./poetry.lock ./poetry.lock
#RUN /root/.local/bin/poetry install
RUN /root/.local/bin/poetry config virtualenvs.create false --local && /root/.local/bin/poetry install
COPY ./todo_app ./todo_app/
RUN ["/root/.local/bin/poetry", "--version"]
FROM base as PROD
CMD /root/.local/bin/poetry run gunicorn -b "0.0.0.0:${PORT:-8080}" "todo_app.app:create_app()"
FROM base as DEV
CMD ["/root/.local/bin/poetry" , "run", "flask", "run" , "-h", "0.0.0.0"]
FROM base as TEST
COPY ./test ./test
ENTRYPOINT ["/root/.local/bin/poetry", "run", "pytest"]
