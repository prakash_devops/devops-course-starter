import pytest, requests, os
from dotenv import load_dotenv, find_dotenv
from todo_app import app
import mongomock
from todo_app.data.trello_items import create_card


@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    # Create the new app.
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        testApp = app.create_app()
    # Use the app to create a test_client that can be used in our tests.
        with testApp.test_client() as client:
            yield client

def test_index_page(monkeypatch, client):
    # Replace call to requests.get(url) with our own function
    create_card("TO_DO", "Test card")
    response = client.get('/')      
    assert response.status_code == 200
    assert 'Test card' in response.data.decode()    