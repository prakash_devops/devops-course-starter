import pytest
from todo_app.view_model import ViewModel
from todo_app.data.item import Item

@pytest.fixture
def view_model() -> ViewModel:
    item1 = Item(1, 'Model-3', 'TO_DO')
    item2 = Item(2, 'Model-2', 'Doing')
    item3 = Item(3, 'Model-1', 'Done')
    item4 = Item(4, 'Model-1 Exercise', 'Done')
    return ViewModel([item1, item2, item3, item4])

def test_doing_items(view_model : ViewModel):
    items = view_model.doing_items
    assert len(items) == 1

def test_todo_items(view_model : ViewModel):
    items = view_model.to_do_items
    assert len(items) == 1

def test_done_items(view_model : ViewModel):
    items = view_model.done_items
    assert len(items) == 2        