class ViewModel:
    def __init__(self, items):
        self.cards = items

    @property
    def items(self):
        return self.cards

    @property
    def doing_items(self):
        doing_items_list = []
        for item in self.items:
            if item.status == 'Doing':
                doing_items_list.append(item)

        return doing_items_list

    @property
    def to_do_items(self):
        todo_items_list = []
        for item in self.items:
            if item.status == 'TO_DO':
                todo_items_list.append(item)

        return todo_items_list

    @property
    def done_items(self):
        done_items_list = []
        for item in self.items:
            if item.status == 'Done':
                done_items_list.append(item)

        return done_items_list        