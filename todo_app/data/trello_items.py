import requests
import os
from todo_app.data.item import Item
import logging
import pymongo
from bson import ObjectId

logging.basicConfig(level=logging.INFO)

def get_cards_details():
    """
    payload = {'cards':'open', 'key': os.getenv('TRELLO_KEY'), 'token': os.getenv('TRELLO_TOKEN')}
    logging.info(f"Board Id {os.getenv('TRELLO_BOARD_ID')}")
    url = 'https://api.trello.com/1/boards/' + os.getenv('TRELLO_BOARD_ID') + '/lists'
    
    r = requests.get(url, params=payload)
    logging.info(f'Url: {r.url}')
    json_data = r.json()
    """
    client = pymongo.MongoClient(os.getenv('MONGODB_CONNECTION_STRING'))
    db = client[os.getenv('DB_NAME')]
    collection = db["TodoItems"]
    json_data = collection.find()
    items = []
    for mongo_item in json_data:
        item = Item.from_mongo_card(mongo_item)
        items.append(item)
    
    return items

def create_card(status, cardName):
    """
    payload = {'idList':listId, 'name' : cardName, 'key': os.getenv('TRELLO_KEY'), 'token': os.getenv('TRELLO_TOKEN')}
    url = 'https://api.trello.com/1/cards'
    r = requests.post(url, data=payload)
    
    logging.info(f'Create Card Url: {r.url}')    
    card = r.json()
    """
    client = pymongo.MongoClient(os.getenv('MONGODB_CONNECTION_STRING'))
    db = client[os.getenv('DB_NAME')]
    collection = db["TodoItems"]
    collection.insert_one({"name": cardName , "status": status })
    
    #return card

def update_card(cardId, status):
    logging.info(f'Moving CardId {cardId} To {status} state')   
    """
    payload = {'idList':listId, 'key': os.getenv('TRELLO_KEY'), 'token': os.getenv('TRELLO_TOKEN')}
    url = 'https://api.trello.com/1/cards/' + cardId
    r = requests.put(url, data=payload)
    logging.info(f'Update_Card Url: {r.url}')
    card = r.json()
    return card    
    """
    client = pymongo.MongoClient(os.getenv('MONGODB_CONNECTION_STRING'))
    db = client[os.getenv('DB_NAME')]
    collection = db["TodoItems"]
    myQuery = {"_id": ObjectId(cardId)}
    newValues = {"$set": {"status": status}}
    collection.update_one(myQuery, newValues)
