from flask import Flask, render_template, request, redirect, url_for
from todo_app.data.session_items import get_items, save_item, add_item
from todo_app.flask_config import Config
from todo_app.data.trello_items import get_cards_details, create_card, update_card
import logging
import os

from todo_app.view_model import ViewModel
logging.basicConfig(level=logging.INFO)

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())
    

    @app.route('/')
    def index():
        items = get_cards_details()
        item_view_model = ViewModel(items)
        logging.info(f'Toal number of Cards : {len(items)}')
        return render_template('index.html', view_model=item_view_model)

    @app.route('/create_card', methods=['POST'])
    def add_card():
        name = request.form['cardName']
        logging.info(f'Adding Card with Name  {name}')
        #idList = os.getenv('TO_DO_IDLIST')
        status = "TO_DO"
        item = create_card(status, name)
        return redirect(url_for('index'))     

    @app.route('/complete_item/<cardId>', methods=['POST'])
    def move_card(cardId):
        logging.info(f'Moving CardId {cardId} To Done state')   
        #idList = os.getenv('DONE_IDLIST')
        status = "Done"
        item = update_card(cardId, status)
        return redirect(url_for('index'))

    return app    
