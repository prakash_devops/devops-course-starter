# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

To get started, you’ll need an API key. You can get your API key by logging into Trello and visiting https://trello.com/app-key
 
You have to generate a token for yourself. On the same page where you found your API key (https://trello.com/app-key), click the hyperlinked "Token" under the API key to get the token.

Replace the below Properties in the .env file 
**TRELLO_KEY**
**TRELLO_TOKEN**
**TRELLO_BOARD_ID**
**TO_DO_IDLIST**
**DONE_IDLIST**

------ Testing ------------
for running the tests, install pytest by running 'poetry add pytest' from project root directory.
Once pytest is installed run the pytest command from project root directory to run all the tests in the test folder.

```bash
$ poetry add pytest --dev
```

******** Running Ansible to install and run the to-do app from Managed server ***********
1) Creeate directory name /home/ec2-user/prakash/ on the Controller Server
2) Copy the files ansible-playbook/to-do-app-playbook.yml and ansible-playbook/to-do-app-inventory.ini from Project directory to Controller Server path /home/ec2-user/prakash/.
3) Copy the file .env.j2 from Project directory to Controller Server path /home/ec2-user/prakash/.
4) run the cmd ansible-playbook to-do-app-playbook.yml -i to-do-app-inventory.ini from Controller Server path /home/ec2-user/prakash/.

# cmd to build todo_app Dev target docker image
docker build --tag todo_app:dev --target DEV .

# cmd to run the todo_app 
docker run --env-file ./.env -p 5100:5000 -d --mount type=bind,source="$(pwd)"/todo_app,target=/opt/todoapp/todo_app todo_app:dev


# cmd to build todo_app test target docker image
docker build --tag todo_app:test --target TEST .

# cmd to run tests of todo_app docker image
docker run todo_app:test test

# cmd to publish the todo_app 
docker login
docker tag todo_app:test prakashpanchadi/todo_app_test
docker push prakashpanchadi/todo_app_test

# todo_app can be accessed using the below link once its deployed to Heroku 
http://todo-app-prakash-02.herokuapp.com/

# cmd to build todo_app PROD target docker image
docker build --tag prakashpanchadi/todo_app_prod --target PROD .

# cmd to run the todo_app Prod image
docker run --env-file ./.env -p 5100:8080  prakashpanchadi/todo_app_prod:latest

# todo_app can be accessed by using the below link once its successfully deployed to Azure web app.
https://prakashtodo-app.azurewebsites.net/

#create CosmoDB Account
az cosmosdb create --name <cosmos_account_name> --resource-group <resource_group_name> --kind MongoDB --capabilities EnableServerless --server-version 3.6

# Create MongoDB database under CosmoDB account
az cosmosdb mongodb database create --account-name <cosmos_account_name> --name <database_name> --resource-group <resource_group_name>

** Add below 2 properties in Azure Portal -> App Service -> Settings -> Configuration -> Application Settings **
MONGODB_CONNECTION_STRING
DB_NAME
